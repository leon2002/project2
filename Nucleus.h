#pragma once
#include <iostream>

class Gene
{
	private:
		unsigned int _start;
		unsigned int _end;
		bool _on_complementary_dna_strand;


	public:
		void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
		
		int get_start_of_gen() const;
		int get_end_of_gen() const;
		bool is_on_complementary_dna_strand() const;

		void set_start_of_gen(unsigned int start);
		void set_end_of_gen(unsigned int end);
		void set_on_complementary_dna_strand(bool on_complementary_dna_strand);





};





class Nucleus 
{
	private:
		std::string _DNA_strand;
		std::string _complementary_DNA_strand;


	public:
		void init(const std::string dna_sequence);

		std::string get_RNA_transcript(const Gene& gene) const;
		std::string get_reversed_DNA_strand() const;
		unsigned int get_num_of_codon_appearances(const std::string& codon) const;








};

