#include "Nucleus.h"
#include <iostream>
#include <string>


//Gene
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand) {
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;

}
int Gene::get_start_of_gen() const {
	return this->_start;

}
int Gene::get_end_of_gen() const {
	return this->_end;

}
bool Gene::is_on_complementary_dna_strand() const {
	return this->_on_complementary_dna_strand;
}
void Gene::set_start_of_gen(unsigned int start)  {
	this->_start = start;

}
void Gene::set_end_of_gen(unsigned int end)  {
	this->_end = end;

}
void Gene::set_on_complementary_dna_strand(bool on_complementary_dna_strand) {
	this->_on_complementary_dna_strand = on_complementary_dna_strand;

}


//Nucleus
void Nucleus::init(const std::string dna_sequence) const {
	std::string s = "";
	int i = 0;
	for (i = 0; i < dna_sequence.length(); i++) {
		if (dna_sequence[i] != 'T' && dna_sequence[i] != 'C' && dna_sequence[i] != 'G' && dna_sequence[i] != 'A') {
			std::cerr << "Invalid character!";
			_exit(1);

		}
	}


	this->_DNA_strand = dna_sequence;
	for (i = 0; i < dna_sequence.length(); i++) {
		if (dna_sequence[i] == 'T') {
			s += "A";

		}
		else if (dna_sequence[i] == 'C') {
			s += "G";

		}
		else if (dna_sequence[i] == 'G') {
			s += "c";

		}
		else if (dna_sequence[i] == 'A') {
			s += "T";

		}
	}
	this->_complementary_DNA_strand = s;

}
std::string Nucleus::get_RNA_transcript(const Gene& gene) const {
	std::string dnaToUse = "";
	if (gene.is_on_complementary_dna_strand()) {
		dnaToUse = this->_complementary_DNA_strand;
	
	}
	else {
		dnaToUse = ;
	}




}
std::string Nucleus::get_reversed_DNA_strand() const {



}
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const {



}
